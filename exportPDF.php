<?php
	function strip_tags_content($text, $tags = '', $invert = FALSE){ 
		preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags); 
		$tags = array_unique($tags[1]); 
		if(is_array($tags) AND count($tags) > 0){ 
			if($invert == FALSE){ 
				return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text); 
			} 
			else{ 
				return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text); 
			} 
		} 
		elseif($invert == FALSE){ 
			return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text); 
		}
		
		return $text; 
	}

	require_once '../../redcap_connect.php';

	$lang = $_GET['lang'];
	$pid = $_GET['pid'];
	$record = $_GET['id'];
	$page = $_GET['page'];
	
	//language
	$query = "SELECT element_enum, element_type, element_validation_type FROM redcap_metadata
		WHERE project_id = " . $pid . " 
		AND field_name LIKE 'languages'";
	$result = mysqli_query($conn, $query);
	$row = mysqli_fetch_array($result);
		
	$tmp = explode(' \n ', $row['element_enum']);
	foreach($tmp AS $key => $value){
		$tmp2 = explode(',', $value);
		if($tmp2[0] == $lang){
			$response['language'] = trim($tmp2[1]);
			break;
		}
	}
	
	//translations
	$query = "SELECT field_name, element_type, misc, grid_name, element_validation_type, element_label FROM redcap_metadata 
		WHERE project_id = " . $pid . " AND form_name LIKE '" . $page . "' 
			AND field_name NOT LIKE 'survey_text%' 
			AND field_name NOT LIKE 'languages' 
			AND field_name NOT LIKE 'survey_instructions'
		ORDER BY field_order";
	$result = mysqli_query($conn, $query);

	while($row = mysqli_fetch_array($result)){
		$misc = explode(PHP_EOL, $row['misc']);

		foreach($misc AS $key => $value){
			//questions
			if(strpos($value, '@p1000lang') !== false){
				$value = str_replace('@p1000lang', '', $value);
				$value = json_decode($value, true);
				foreach($value AS $key2 => $trans){
					if($key2 == $response['language']){
						$response['questions'][$row['field_name']]['text'] = strip_tags($trans);
						if(strpos($row['element_validation_type'], 'date') !== false){
							$response['questions'][$row['field_name']]['type'] = 'date';
						}
						else{
							$response['questions'][$row['field_name']]['type'] = $row['element_type'];
						}
						$response['questions'][$row['field_name']]['matrix'] = $row['grid_name'];
					}
				}
			}
			//answers
			elseif(strpos($value, '@p1000answers') !== false){
				$value = str_replace('@p1000answers', '', $value);
				$value = json_decode($value, true);
				foreach($value AS $key2 => $trans){
					if($key2 == $response['language']){
						$response['answers'][$row['field_name']]['text'] = $trans;
						if(strpos($row['element_validation_type'], 'date') !== false){
							$response['answers'][$row['field_name']]['type'] = 'date';
						}
						elseif(strpos($row['element_validation_type'], 'signature') !== false){
							$response['answers'][$row['field_name']]['type'] = 'signature';
						}
						else{
							$response['answers'][$row['field_name']]['type'] = $row['element_type'];
						}
						$response['answers'][$row['field_name']]['matrix'] = $row['grid_name'];
					}
				}
			}
		}
		
		//non translated fields
		if(!isset($response['questions'][$row['field_name']])){
			$response['questions'][$row['field_name']]['text'] = $row['element_label'];
			$response['questions'][$row['field_name']]['type'] = 'text';
		}
	}
	
	//data
	$query = "SELECT record, field_name, instance, value FROM redcap_data WHERE project_id = " . $pid . 
		" AND record LIKE '" . $record . "'";
	$result = mysqli_query($conn, $query);
	
	while($row = mysqli_fetch_array($result)){
		if($response['questions'][$row['field_name']]['type'] == 'checkbox'){
			$myData[$row['record']][($row['instance'] == null ? 1 : $row['instance'])][$row['field_name'] . '___' . $row['value']] = 1;
		}
		elseif($image){
			//signatures
			
		}
		else{
			$myData[$row['record']][($row['instance'] == null ? 1 : $row['instance'])][$row['field_name']] = $row['value'];
		}
	}
	
	//echo json_encode($response);
	echo '<img src="/temp/20170620111114_pid4279_JJhrmQ.png">';
	echo json_encode($myData);
	
	//export
	/*
	header("Content-type: text/csv");
	header("Content-Disposition: attachment; filename=\"" . REDCap::getProjectTitle() . " DATA (" . $response['language'] . ") " . date('Y-m-d Hi') . ".csv\"");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo $data;
	*/

?>